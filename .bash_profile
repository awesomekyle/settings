export ANDROID_SDK=~/src/android-sdk
export ANDROID_NDK=$ANDROID_SDK/android-ndk
export PATH=/usr/local/bin:$ANDROID_SDK/tools:$ANDROID_SDK/platform-tools:$ANDROID_NDK:$PATH

source `brew --repository`/Library/Contributions/brew_bash_completion.sh
